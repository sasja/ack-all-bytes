# ack-all-bytes [![codecov](https://codecov.io/gl/Sasja/ack-all-bytes/branch/master/graph/badge.svg)](https://codecov.io/gl/Sasja/ack-all-bytes)

## what?

add reliable delivery to an ordered and error-checked stream of bytes

## use case
reliable serial over USB using cheap 10m USB cables.

## what's the protocol?
![](doc/protocol1.jpg)
![](doc/protocol2.jpg)
(D0, D1, D2 is just shorthand for DATA0, DATA1, DATA2)  
(not on the pic, but for a 2 byte payload the sender just sends D2 too instead of only D0 and D1)