#include "unity.h"
#include "ackallbytes.h"
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

#define MOCK_STREAM_MAX_LENGTH 1024
struct mock_stream
{
    int n_in;
    uint8_t indata[MOCK_STREAM_MAX_LENGTH];
    int n_out;
    uint8_t outdata[MOCK_STREAM_MAX_LENGTH];
};

int mock_fgetc(void * f)
{
    struct mock_stream *ms = (struct mock_stream *) f;
    assert(ms->n_in >= 0);
    if(ms->n_in == 0) return -1;
    ms->n_in--;
    return ms->indata[ms->n_in];
}

int mock_fputc(void * f, char c)
{
    struct mock_stream *ms = (struct mock_stream *) f;
    ms->outdata[ms->n_out] = c;
    assert(ms->n_out < MOCK_STREAM_MAX_LENGTH);
    ms->n_out++;
}

void pass_mock_stream(
        struct mock_stream *send, struct mock_stream *recv,
        float success_rate)
{
    recv->n_in = 0;
    while(send->n_out > 0)
    {
        send->n_out--;
        if((float)rand() / RAND_MAX <= success_rate)
        {
            recv->indata[recv->n_in] = send->outdata[send->n_out];
            recv->n_in++;
        }
    }
}

void mock_reset_timer(void * timer)
{
    int *cnt = (int*) timer;
    *cnt = 1;
}

int mock_is_timeout(void * timer)
{
    int *cnt = (int*) timer;
    if(*cnt > 0)
    {
        (*cnt)--;
        return 0;
    }
    else
    {
        return 1;
    }
}

void nop(void * timer) { }
int always_timeout(void * timer) { return 1; }
int never_timeout(void * timer) { return 0; }

/* ============================================================================= */

void test_aab_init(void)
{
    struct ackallbytes aab;
    aab_init(&aab,
            NULL, NULL, NULL,
            NULL, NULL, NULL,
            NULL, NULL, NULL);
    TEST_ASSERT_EQUAL_INT(-1, aab.data0);
    TEST_ASSERT_EQUAL_INT(-1, aab.data1);
    TEST_ASSERT_EQUAL_INT(-1, aab.data2);
    TEST_ASSERT_EQUAL_INT(0, aab.payload_forwarded);

    TEST_ASSERT_EQUAL_INT(-1, aab.sending0);
    TEST_ASSERT_EQUAL_INT(-1, aab.sending1);
    TEST_ASSERT_EQUAL_INT(-1, aab.sending2);
    TEST_ASSERT_EQUAL_INT(-1, aab.expected_ack);
}

void test_no_output_when_no_input(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {0, {}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(0, bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, good.n_out);
}

void test_single_byte_payload_transmit(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {1, {'A'}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(2, bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, good.n_in);
}

void test_double_byte_payload_transmit(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {2, {'A','B'}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(3, bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, good.n_in);
}

void test_only_resend_on_timeout(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {1, {'A'}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};
    int tmp;

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_handle(&aab);
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(2, bad.n_out);

    /* reset streams */
    good.n_in = 1;
    bad.n_out = 0;

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, always_timeout);

    aab_handle(&aab);
    tmp = bad.n_out;
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(2, bad.n_out - tmp);
}

void test_dont_send_next_without_ack(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {3, {'A','B','C'}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(3, bad.n_out);
    TEST_ASSERT_EQUAL_INT(1, good.n_in);

    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(3, bad.n_out);
    TEST_ASSERT_EQUAL_INT(1, good.n_in);
}

void test_send_next_on_ack(void)
{
    struct ackallbytes aab;
    struct mock_stream good = {3, {'A','B','C'}, 0, {}};
    struct mock_stream bad = {0, {}, 0, {}};

    aab_init(&aab,
            &good, mock_fgetc, mock_fputc,
            &bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(3, bad.n_out);
    TEST_ASSERT_EQUAL_INT(1, good.n_in);

    bad.n_in = 1;
    bad.indata[0] = 0xC0; /* should start with a seq = 0 ack */
    aab_handle(&aab);

    TEST_ASSERT_EQUAL_INT(5, bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, good.n_in);
}

void test_decode_onebyte_payload(void)
{
    struct ackallbytes send, recv;
    struct mock_stream s_good = {1, {'A'}, 0, {}};
    struct mock_stream s_bad = {0, {}, 0, {}};
    struct mock_stream r_good = {0, {}, 0, {}};
    struct mock_stream r_bad = {0, {}, 0, {}};

    aab_init(&send,
            &s_good, mock_fgetc, mock_fputc,
            &s_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_init(&recv,
            &r_good, mock_fgetc, mock_fputc,
            &r_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_handle(&send);

    TEST_ASSERT_EQUAL_INT(2, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, s_good.n_in);

    pass_mock_stream(&s_bad, &r_bad, 1.0);

    TEST_ASSERT_EQUAL_INT(0, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(2, r_bad.n_in);

    aab_handle(&recv); /* should handle all incomming */

    /* decoded payload */
    TEST_ASSERT_EQUAL_INT(1, r_good.n_out);
    TEST_ASSERT_EQUAL_HEX8('A', r_good.outdata[0]);
    /* ACK */
    TEST_ASSERT_EQUAL_INT(1, r_bad.n_out);
    TEST_ASSERT_EQUAL_HEX8(0xC0, r_bad.outdata[0]);
}

void test_decode_twobytes_payload(void)
{
    struct ackallbytes send, recv;
    struct mock_stream s_good = {2, {'B', 'A'}, 0, {}};
    struct mock_stream s_bad = {0, {}, 0, {}};
    struct mock_stream r_good = {0, {}, 0, {}};
    struct mock_stream r_bad = {0, {}, 0, {}};

    aab_init(&send,
            &s_good, mock_fgetc, mock_fputc,
            &s_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_init(&recv,
            &r_good, mock_fgetc, mock_fputc,
            &r_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_handle(&send);

    TEST_ASSERT_EQUAL_INT(3, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, s_good.n_in);

    pass_mock_stream(&s_bad, &r_bad, 1.0);

    TEST_ASSERT_EQUAL_INT(0, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(3, r_bad.n_in);

    aab_handle(&recv); /* should handle all incomming */

    /* decoded payload */
    TEST_ASSERT_EQUAL_INT(2, r_good.n_out);
    TEST_ASSERT_EQUAL_HEX8('A', r_good.outdata[0]);
    TEST_ASSERT_EQUAL_HEX8('B', r_good.outdata[1]);
    /* ACK */
    TEST_ASSERT_EQUAL_INT(1, r_bad.n_out);
    TEST_ASSERT_EQUAL_HEX8(0xC0, r_bad.outdata[0]);
}

void test_transmit_string(void)
{
    int i;
    struct ackallbytes send, recv;
    struct mock_stream s_good = {5, {'o', 'l', 'l', 'e', 'H'}, 0, {}};
    struct mock_stream s_bad = {0, {}, 0, {}};
    struct mock_stream r_good = {0, {}, 0, {}};
    struct mock_stream r_bad = {0, {}, 0, {}};

    aab_init(&send,
            &s_good, mock_fgetc, mock_fputc,
            &s_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    aab_init(&recv,
            &r_good, mock_fgetc, mock_fputc,
            &r_bad,  mock_fgetc, mock_fputc,
            NULL, nop, never_timeout);

    for(i = 0; i < 20; i++) /* loop enough */
    {
        aab_handle(&recv); /* empties r_bad */
        pass_mock_stream(&s_bad, &r_bad, 1.0); /* feeds it */

        aab_handle(&send); /* empties s_bad */
        pass_mock_stream(&r_bad, &s_bad, 1.0); /* feeds it */
    }

    /* tranmission completed */
    TEST_ASSERT_EQUAL_INT(0, s_good.n_in);
    TEST_ASSERT_EQUAL_INT(0, s_good.n_out);
    TEST_ASSERT_EQUAL_INT(0, s_bad.n_in);
    TEST_ASSERT_EQUAL_INT(0, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, r_good.n_in);
    TEST_ASSERT_EQUAL_INT(5, r_good.n_out); /* 'Hello' */
    TEST_ASSERT_EQUAL_INT(0, r_bad.n_in);
    TEST_ASSERT_EQUAL_INT(0, r_bad.n_out);

    /* decoded stream */
    TEST_ASSERT_EQUAL_HEX8('H', r_good.outdata[0]);
    TEST_ASSERT_EQUAL_HEX8('e', r_good.outdata[1]);
    TEST_ASSERT_EQUAL_HEX8('l', r_good.outdata[2]);
    TEST_ASSERT_EQUAL_HEX8('l', r_good.outdata[3]);
    TEST_ASSERT_EQUAL_HEX8('o', r_good.outdata[4]);
}

void test_long_random_lossy(void)
{
    int i;
    uint8_t testdata[MOCK_STREAM_MAX_LENGTH];
    struct ackallbytes send, recv;
    struct mock_stream s_good = {0, {}, 0, {}};
    struct mock_stream s_bad = {0, {}, 0, {}};
    struct mock_stream r_good = {0, {}, 0, {}};
    struct mock_stream r_bad = {0, {}, 0, {}};
    int s_timer = 1;
    int r_timer = 1;

    aab_init(&send,
            &s_good, mock_fgetc, mock_fputc,
            &s_bad,  mock_fgetc, mock_fputc,
            &s_timer, mock_reset_timer, mock_is_timeout);

    aab_init(&recv,
            &r_good, mock_fgetc, mock_fputc,
            &r_bad,  mock_fgetc, mock_fputc,
            &r_timer, mock_reset_timer, mock_is_timeout);

    /* generate and post test data for transmission */
    for(i = 0; i < MOCK_STREAM_MAX_LENGTH; i++)
    {
        testdata[i] = rand() % 256;
        s_good.indata[MOCK_STREAM_MAX_LENGTH - 1 - i] = testdata[i];
    }
    s_good.n_in = MOCK_STREAM_MAX_LENGTH;

    for(i = 0; i < MOCK_STREAM_MAX_LENGTH * 10; i++) /* just loop enough */
    {
        aab_handle(&recv); /* empties r_bad */
        pass_mock_stream(&s_bad, &r_bad, 0.66); /* feeds it */

        aab_handle(&send); /* empties s_bad */
        pass_mock_stream(&r_bad, &s_bad, 0.66); /* feeds it */
    }

    /* tranmission completed */
    TEST_ASSERT_EQUAL_INT(0, s_good.n_in);
    TEST_ASSERT_EQUAL_INT(0, s_good.n_out);
    TEST_ASSERT_EQUAL_INT(0, s_bad.n_in);
    TEST_ASSERT_EQUAL_INT(0, s_bad.n_out);
    TEST_ASSERT_EQUAL_INT(0, r_good.n_in);
    TEST_ASSERT_EQUAL_INT(MOCK_STREAM_MAX_LENGTH, r_good.n_out);
    TEST_ASSERT_EQUAL_INT(0, r_bad.n_in);
    TEST_ASSERT_EQUAL_INT(0, r_bad.n_out);

    /* decoded stream */
    TEST_ASSERT_EQUAL_HEX8_ARRAY(
            testdata, r_good.outdata,MOCK_STREAM_MAX_LENGTH);
}

int main(void)
{
    UNITY_BEGIN();

    RUN_TEST(test_aab_init);
    RUN_TEST(test_no_output_when_no_input);
    RUN_TEST(test_single_byte_payload_transmit);
    RUN_TEST(test_double_byte_payload_transmit);
    RUN_TEST(test_only_resend_on_timeout);
    RUN_TEST(test_dont_send_next_without_ack);
    RUN_TEST(test_send_next_on_ack);

    RUN_TEST(test_decode_onebyte_payload);
    RUN_TEST(test_decode_twobytes_payload);

    RUN_TEST(test_transmit_string);
    RUN_TEST(test_long_random_lossy);

    return UNITY_END();
}
