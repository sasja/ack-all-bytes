#include "ackallbytes.h"
#include <stdio.h>

static int is_data0(int packet);
static int is_data0_onebyte(int packet);
static int is_data0_twobytes(int packet);
static int is_data1(int packet);
static int is_data2(int packet);
static int payload_valid(struct ackallbytes *aab);
static void forward_payload(struct ackallbytes *aab);
static void ack(struct ackallbytes *aab);
static void check_for_and_encode_new_data(struct ackallbytes *aab);
static void transmit(struct ackallbytes *aab);

/* ========================================================================== */

void aab_init(
        struct ackallbytes * aab,

        void * good_stream,
        int (*good_fgetc)(void *),
        int (*good_fputc)(void *, char),

        void * bad_stream,
        int (*bad_fgetc)(void *),
        int (*bad_fputc)(void *, char),

        void * timer,
        void (*reset_timer)(void *),
        int (*is_timeout)(void *)
        )
{
    aab->good_stream = good_stream;
    aab->good_fgetc = good_fgetc;
    aab->good_fputc = good_fputc;

    aab->bad_stream = bad_stream;
    aab->bad_fgetc = bad_fgetc;
    aab->bad_fputc = bad_fputc;

    aab->timer = timer;
    aab->reset_timer = reset_timer;
    aab->is_timeout = is_timeout;

    aab->data0 = -1;
    aab->data1 = -1;
    aab->data2 = -1;
    aab->payload_forwarded = 0;

    aab->sending0 = -1;
    aab->sending1 = -1;
    aab->sending2 = -1;
    aab->expected_ack = -1;
}

void aab_handle(struct ackallbytes * aab)
{
    int check_data_to_ack = 0;

    /* handle all incoming packets */
    int packet = aab->bad_fgetc(aab->bad_stream);
    while(packet != AAB_EOF)
    {
        if(packet == aab->expected_ack)
        {
            aab->expected_ack = -1;
            /* this signals we can proceed with sending the next byte */
        }
        else if(is_data0(packet))
        {
            if(packet != aab->data0) /* a new DATA0 appeared! */
            {
                aab->data0 = packet;
                aab->data1 = -1;
                aab->data2 = -1;
                aab->payload_forwarded = 0;
            }
            else
            {
                check_data_to_ack = 1;
            }
        }
        else if(is_data1(packet))
        {
            aab->data1 = packet;
            check_data_to_ack = 1;
        }
        else if(is_data2(packet))
        {
            aab->data2 = packet;
            check_data_to_ack = 1;
        }
        packet = aab->bad_fgetc(aab->bad_stream);
    }

    /* when data packet received, ack if valid payload and forward if new */
    if(check_data_to_ack) 
    {
        if(payload_valid(aab))
        {
            if(!aab->payload_forwarded) forward_payload(aab);
            ack(aab);
        }
    }

    /* prepare and start new data transmission */
    if(aab->expected_ack == -1) /* last transmission has been acked */
    {
        check_for_and_encode_new_data(aab);
        if(aab->expected_ack != -1) /* we got a new transmission ready */
        {
            transmit(aab);
            aab->reset_timer(aab->timer);
        }
    }
    else /* waiting for ack */
    {
        if(aab->is_timeout(aab->timer))
        {
            transmit(aab);
        }
    }

}

/* ========================================================================== */

static int is_data0(int packet)
{
    return (packet & 0xC0) == 0x00; /* 0b00xxxxxx */
}

static int is_data0_onebyte(int packet)
{
    return (packet & 0xE0) == 0x00; /* 0b000xxxxx */
}

static int is_data0_twobytes(int packet)
{
    return (packet & 0xE0) == 0x20; /* 0b001xxxxx */
}

static int is_data1(int packet)
{
    return (packet & 0xC0) == 0x40; /* 0b01xxxxxx */
}

static int is_data2(int packet)
{
    return (packet & 0xC0) == 0x80; /* 0b10xxxxxx */
}

static int payload_valid(struct ackallbytes *aab)
{
    int valid = 0;
    if( 
            is_data0_onebyte(aab->data0) &&
            aab->data1 != -1)
    {
        valid = 1;
    }
    else if(
            is_data0_twobytes(aab->data0) &&
            aab->data1 != -1 &&
            aab->data2 != -1)
    {
        valid = 1;
    }
    return valid;
}

static void forward_payload(struct ackallbytes *aab)
{
    int payl;

    payl = ((aab->data0 << 4) & 0xF0) |
           ((aab->data1 >> 2) & 0x0F);
    aab->good_fputc(aab->good_stream, payl);

    if(is_data0_twobytes(aab->data0))
    {
        payl = ((aab->data1 << 6) & 0xC0) |
               (aab->data2 & 0x3F);
        aab->good_fputc(aab->good_stream, payl);
    }

    aab->payload_forwarded = 1;
}

static void ack(struct ackallbytes *aab)
{
    int seq = (aab->data0 >> 4) & 0x01;
    char ack =
        0xC0 |
        seq << 5;
    aab->bad_fputc(aab->bad_stream, ack);
}

static void check_for_and_encode_new_data(struct ackallbytes *aab)
{
    int payl0, payl1, seq;
    int length_bit;

    payl0 = aab->good_fgetc(aab->good_stream);
    if(payl0 != AAB_EOF)
    {
        payl1 = aab->good_fgetc(aab->good_stream);
    }
    else
    {
        payl1 = AAB_EOF;
    }

    if(payl0 != AAB_EOF) /* ok we got new data to transmit */
    {
        seq = (aab->sending0 >> 4) & 0x01;
        seq = (seq + 1) % 2;
        /* seq wil first be 0 due sending0 = -1 initialisation */
        length_bit = payl1 != AAB_EOF ? 1 : 0;

        aab->sending0 =
            0x00 |
            (length_bit << 5) |
            (seq << 4) |
            ((payl0 & 0xF0) >> 4);

        aab->sending1 = 
            0x40 |
            ((payl0 & 0x0F) << 2);
        
        if(payl1 != AAB_EOF) /* its a two byte transmission */
        {
            aab->sending1 |= (payl1 & 0xC0) >> 6;

            aab->sending2 =
                0x80 |
                (payl1 & 0x3F);
        }
        else
        {
            aab->sending2 = -1;
        }

        aab->expected_ack =
            0xC0 |
            seq << 5;
    }
}

static void transmit(struct ackallbytes *aab)
{
    aab->bad_fputc(aab->bad_stream, aab->sending0);
    aab->bad_fputc(aab->bad_stream, aab->sending1);
    if(aab->sending2 != -1) aab->bad_fputc(aab->bad_stream, aab->sending2);
}
