#ifndef _ACKALLBYTES_H
#define _ACKALLBYTES_H

#define AAB_EOF -1

struct ackallbytes
{
    void * good_stream;
    int (*good_fgetc)(void *);
    int (*good_fputc)(void *, char);

    void * bad_stream;
    int (*bad_fgetc)(void *);
    int (*bad_fputc)(void *, char);

    void * timer;
    void (*reset_timer)(void *);
    int (*is_timeout)(void *);

    int data0;
    int data1;
    int data2;
    int payload_forwarded;

    int sending0;
    int sending1;
    int sending2;
    int expected_ack;
};

extern void aab_init(
        struct ackallbytes * aab,

        void * good_stream,
        int (*good_fgetc)(void *),
        int (*good_fputc)(void *, char),

        void * bad_stream,
        int (*bad_fgetc)(void *),
        int (*bad_fputc)(void *, char),

        void * timer,
        void (*reset_timer)(void *),
        int (*is_timeout)(void *)
        );

extern void aab_handle(struct ackallbytes * aab);

#endif /* _ACKALLBYTES_H */
